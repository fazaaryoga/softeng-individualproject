package SoftEngIndividualProject.ProjectApplication.controller;

import SoftEngIndividualProject.ProjectApplication.core.User;
import SoftEngIndividualProject.ProjectApplication.form.UserRegisForm;
import SoftEngIndividualProject.ProjectApplication.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class UserController {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserService userService;

    @GetMapping("/register")
    public String showRegisterPage(Model model){
        model.addAttribute("form", new UserRegisForm());
        return "register";
    }

    @PostMapping("/")
    public String registerUser(@ModelAttribute("form") UserRegisForm userRegisForm, Model model){
        try {
            userService.registerUser(userRegisForm);
            model.addAttribute("message", "Account Created!");
            return "home";
        }catch (Exception E){
            model.addAttribute("message", "Username unavailable please another one");
            return "/register";
        }

    }

}
