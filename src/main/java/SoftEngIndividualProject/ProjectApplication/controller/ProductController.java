package SoftEngIndividualProject.ProjectApplication.controller;

import SoftEngIndividualProject.ProjectApplication.core.Product;
import SoftEngIndividualProject.ProjectApplication.service.ProductService;
import org.dom4j.rule.Mode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class ProductController {

    @Autowired
    private ProductService productService;

    @GetMapping("/product")
    public String ShowCatalogPage(Model model, @RequestParam(defaultValue = "all") String component, @RequestParam(defaultValue = "asc") String sort){
        if (component.equals("all")) {
            model.addAttribute("graphics_cards", productService.getProductsByCategorySortBy("graphics_card", sort));
            model.addAttribute("motherboards", productService.getProductsByCategorySortBy("motherboards", sort));
            model.addAttribute("processors", productService.getProductsByCategorySortBy("processors", sort));
            model.addAttribute("psus", productService.getProductsByCategorySortBy("psu", sort));
            model.addAttribute("rams", productService.getProductsByCategorySortBy("ram", sort));
            model.addAttribute("memorys", productService.getProductsByCategorySortBy("memory", sort));
        }else if (component.equals("cpu")){
            model.addAttribute("processors", productService.getProductsByCategorySortBy("processors", sort));
        }else if (component.equals("gpu")){
            model.addAttribute("graphics_cards", productService.getProductsByCategorySortBy("graphics_card", sort));
        }else if (component.equals("mobo")){
            model.addAttribute("motherboards", productService.getProductsByCategorySortBy("motherboards", sort));
        }else if (component.equals("ram")){
            model.addAttribute("rams", productService.getProductsByCategorySortBy("ram", sort));
        }else if (component.equals("hdd")){
            model.addAttribute("memorys", productService.getProductsByCategorySortBy("memory", sort));
        }else if (component.equals("psu")){
            model.addAttribute("psus", productService.getProductsByCategorySortBy("psu", sort));
        }
        return "catalog";
    }

    @GetMapping("/add-product")
    public String showAddProductPage(Model model){
        model.addAttribute("product", new Product());
        return "add";
    }

    @PostMapping("/admin")
    public String addProduct(@ModelAttribute("product") Product product, Model model){
        productService.addProduct(product);
        String message = "Product Added!";
        model.addAttribute("message", message);
        return "admin";
    }


}
