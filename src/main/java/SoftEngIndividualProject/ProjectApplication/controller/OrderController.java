package SoftEngIndividualProject.ProjectApplication.controller;

import SoftEngIndividualProject.ProjectApplication.core.Order;
import SoftEngIndividualProject.ProjectApplication.core.Product;
import SoftEngIndividualProject.ProjectApplication.core.User;
import SoftEngIndividualProject.ProjectApplication.form.OrderProductForm;
import SoftEngIndividualProject.ProjectApplication.service.OrderService;
import SoftEngIndividualProject.ProjectApplication.service.ProductService;
import SoftEngIndividualProject.ProjectApplication.service.UserService;
import org.dom4j.rule.Mode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
public class OrderController {

    @Autowired
    private OrderService orderService;

    @Autowired
    private ProductService productService;

    @Autowired
    private UserService userService;

    @GetMapping("/builder")
    public String showBuilderPage(Model model){
        OrderProductForm form = new OrderProductForm();
        model.addAttribute("graphics_cards", productService.getProductsByCategory("graphics_card"));
        model.addAttribute("motherboards", productService.getProductsByCategory("motherboards"));
        model.addAttribute("processors", productService.getProductsByCategory("processors"));
        model.addAttribute("psus", productService.getProductsByCategory("psu"));
        model.addAttribute("rams", productService.getProductsByCategory("ram"));
        model.addAttribute("memorys", productService.getProductsByCategory("memory"));
        model.addAttribute("form", form);
        return "builder";
    }

    @PostMapping("/checkout")
    public String showCheckoutPage(@ModelAttribute("form") OrderProductForm orderProductForm, Model model, Principal principal){
        Order order = new Order(orderProductForm.getName(), orderProductForm.getAddress());
        order.addProduct(orderService.getProduct(orderProductForm.getCpu_id()));
        order.addProduct(orderService.getProduct(orderProductForm.getGraphics_card_id()));
        order.addProduct(orderService.getProduct(orderProductForm.getMotherboard_id()));
        order.addProduct(orderService.getProduct(orderProductForm.getRam_id()));
        order.addProduct(orderService.getProduct(orderProductForm.getMemory_id()));
        order.addProduct(orderService.getProduct(orderProductForm.getPsu_id()));
        orderService.calculateTotal(order);
        orderService.placeOrder(order);
        try {
            String username = principal.getName();
            userService.addOrder(order, username);
        }catch (Exception e){

        }
        model.addAttribute("order", order);
        return "checkout";
    }

    @GetMapping("/orders")
    public String showOrderPage(Model model){
        model.addAttribute("unc_orders", orderService.getOrderByStatus(false));
        model.addAttribute("orders", orderService.getOrderByStatus(true));
        return "orders";
    }

    @PostMapping("/orders")
    public String completeOrder(@RequestParam(value = "order_id") long id,Model model){
        orderService.completeOrder(id);
        model.addAttribute("unc_orders", orderService.getOrderByStatus(false));
        model.addAttribute("orders", orderService.getOrderByStatus(true));
        return "orders";
    }

    @GetMapping("/my-orders")
    public String showMyOrdersPage(Model model, Principal principal){
        model.addAttribute("orders", userService.getUser(principal.getName()).getOrders());
        return "myorder";
    }
}
