package SoftEngIndividualProject.ProjectApplication.repository;

import SoftEngIndividualProject.ProjectApplication.core.Product;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends CrudRepository<Product, Long> {

    @Query("FROM Product WHERE category = ?1")
    Iterable<Product> findByCategory(String category);

    @Query("FROM Product WHERE category = ?1 order by price asc")
    Iterable<Product> findByCategorySortByAsc(String category);

    @Query("FROM Product WHERE category = ?1 order by price desc")
    Iterable<Product> findByCategorySortByDesc(String category);

}
