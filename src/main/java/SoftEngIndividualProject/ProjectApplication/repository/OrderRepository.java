package SoftEngIndividualProject.ProjectApplication.repository;

import SoftEngIndividualProject.ProjectApplication.core.Order;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository extends CrudRepository<Order, Long> {

    @Query("FROM Order WHERE order_status = ?1")
    Iterable<Order> findByOrderStatus(boolean order_status);
}
