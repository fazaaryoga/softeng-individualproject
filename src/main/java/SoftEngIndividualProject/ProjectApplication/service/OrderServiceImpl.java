package SoftEngIndividualProject.ProjectApplication.service;

import SoftEngIndividualProject.ProjectApplication.core.Order;
import SoftEngIndividualProject.ProjectApplication.core.Product;
import SoftEngIndividualProject.ProjectApplication.form.OrderProductForm;
import SoftEngIndividualProject.ProjectApplication.repository.OrderRepository;
import SoftEngIndividualProject.ProjectApplication.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private ProductRepository productRepository;

    @Override
    public void completeOrder(long id) {
        Order order = orderRepository.findById(id).get();
        order.setOrderStatus(true);
        orderRepository.save(order);

    }

    @Override
    public void calculateTotal(Order order) {
        long total = 0;
        for(Product product: order.getProducts()){
            total += product.getPrice();
        }

        order.setTotal(total);
    }

    @Override
    public void placeOrder(Order order) {
        orderRepository.save(order);
    }

    @Override
    public Product getProduct(long id){
        Iterable<Product> products = productRepository.findAll();
        for(Product product: products){
            if(product.getId() == id){
                return product;
            }
        }
        return null;

    }

    @Override
    public void addProduct(Order order, Product product) {
        order.addProduct(product);
    }

    @Override
    public Iterable<Order> getOrderByStatus(boolean orderStatus) {
        return orderRepository.findByOrderStatus(orderStatus);
    }
}
