package SoftEngIndividualProject.ProjectApplication.service;

import SoftEngIndividualProject.ProjectApplication.core.Order;
import SoftEngIndividualProject.ProjectApplication.core.User;
import SoftEngIndividualProject.ProjectApplication.form.UserRegisForm;

public interface UserService {
    public void registerUser(UserRegisForm userRegisForm) throws Exception;
    public boolean checkUsername(String username);
    public User getUser(String username);
    public void addOrder(Order order, String username);
}
