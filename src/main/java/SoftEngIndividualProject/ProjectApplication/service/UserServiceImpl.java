package SoftEngIndividualProject.ProjectApplication.service;

import SoftEngIndividualProject.ProjectApplication.core.Order;
import SoftEngIndividualProject.ProjectApplication.core.User;
import SoftEngIndividualProject.ProjectApplication.form.UserRegisForm;
import SoftEngIndividualProject.ProjectApplication.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.parameters.P;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public void registerUser(UserRegisForm userRegisForm) throws Exception{
        if (checkUsername(userRegisForm.getUsername())){
            throw new Exception(
                    "username already exist"
            );
        }else {
            User user = new User();
            user.setPassword(passwordEncoder.encode(userRegisForm.getPassword()));
            user.setUsername(userRegisForm.getUsername());
            user.setAuthorization("CUSTOMER");
            userRepository.save(user);
        }
    }

    @Override
    public boolean checkUsername(String username) {
        Optional<User> user = userRepository.findByUsername(username);
        if(user.isPresent()){
            return true;
        }else{
            return false;
        }
    }

    @Override
    public User getUser(String username) {
        return userRepository.findByUsername(username).get();
    }

    @Override
    public void addOrder(Order order, String username) {
        User user = userRepository.findByUsername(username).get();
        user.addOrder(order);
        userRepository.save(user);
    }
}
