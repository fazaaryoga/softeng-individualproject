package SoftEngIndividualProject.ProjectApplication.service;

import SoftEngIndividualProject.ProjectApplication.core.Product;

import java.util.Optional;

public interface ProductService {
    void addProduct(Product product);
    Iterable<Product> getProducts();
    Iterable<Product> getProductsByCategory(String category);
    Optional<Product> getProductById(Long id);
    Iterable<Product> getProductsByCategorySortBy(String category, String sort);
}
