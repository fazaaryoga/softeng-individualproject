package SoftEngIndividualProject.ProjectApplication.service;

import SoftEngIndividualProject.ProjectApplication.core.Order;
import SoftEngIndividualProject.ProjectApplication.core.Product;
import org.springframework.stereotype.Service;

public interface OrderService {
    void completeOrder(long id);
    void calculateTotal(Order order);
    void placeOrder(Order order);
    void addProduct(Order order, Product product);
    Product getProduct(long id);
    Iterable<Order> getOrderByStatus(boolean orderStatus);
}
