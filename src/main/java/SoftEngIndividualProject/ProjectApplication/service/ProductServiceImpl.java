package SoftEngIndividualProject.ProjectApplication.service;

import SoftEngIndividualProject.ProjectApplication.core.Product;
import SoftEngIndividualProject.ProjectApplication.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductRepository productRepository;

    @Override
    public void addProduct(Product product) {
        productRepository.save(product);
    }

    @Override
    public Iterable<Product> getProducts() {
        return productRepository.findAll();
    }

    @Override
    public Iterable<Product> getProductsByCategory(String category) {
        Iterable<Product> products = productRepository.findByCategory(category);
        return products;
    }

    @Override
    public Iterable<Product> getProductsByCategorySortBy(String category, String sort) {
        if (sort.equals("asc")){
            Iterable<Product> products = productRepository.findByCategorySortByAsc(category);
            return products;
        }else{
            Iterable<Product> products = productRepository.findByCategorySortByDesc(category);
            return products;
        }
    }

    @Override
    public Optional<Product> getProductById(Long id) {
        return productRepository.findById(id);
    }
}
