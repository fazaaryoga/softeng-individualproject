package SoftEngIndividualProject.ProjectApplication.core;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "id_generator")
    @SequenceGenerator(name = "id_generator", sequenceName = "id_generator", initialValue = 36, allocationSize = 50)
    private long id;

    @Column(name = "product_name")
    private String name;

    private long price;
    private String information;

    @Column(name = "product_category")
    private String category;


}
