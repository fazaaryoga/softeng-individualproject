package SoftEngIndividualProject.ProjectApplication.core;

import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Entity
@Table(name = "users")
@Data
public class User implements UserDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "userId_generator")
    @SequenceGenerator(name = "userId_generator", sequenceName = "userId_generator", initialValue = 5, allocationSize = 50)
    @Column(name = "user_id")
    private long id;

    @Column(name = "username")
    private String username;

    @Column(name = "password")
    private String password;

    @Column(name = "authorizations")
    private String authorization;

    @Column(name = "locked")
    private boolean accountNonLocked;

    @Column(name = "expired")
    private boolean accountNonExpired;

    @Column(name = "credentials")
    private boolean credentialNonExpired;

    @Column(name = "enabled")
    private boolean enabled;

    @OneToMany
    @JoinTable(name = "user_orders", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "order_id"))
    private List<Order> orders;

    public User(){
        this.accountNonExpired = true;
        this.accountNonLocked = true;
        this.credentialNonExpired = true;
        this.enabled = true;
        this.orders = new ArrayList<Order>();
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return this.credentialNonExpired;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority(this.authorization));
        return authorities;
    }

    public void addOrder(Order order){
        this.orders.add(order);
    }
}
