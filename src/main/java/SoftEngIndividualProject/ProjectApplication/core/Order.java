package SoftEngIndividualProject.ProjectApplication.core;

import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@Table(name = "order_table")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "order_id")
    private long id;

    @Column(name = "order_status")
    private boolean orderStatus = false;

    private String customerName;
    private String address;
    private long total;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "order_products", joinColumns =@JoinColumn(name = "order_id"), inverseJoinColumns = @JoinColumn(name="product_id"))
    private List<Product> products;

    public  Order(){}

    public Order(String customerName, String address){
        this.customerName = customerName;
        this.address = address;
        this.products = new ArrayList<Product>();
        this.total = 0;


    }

    public void addProduct(Product product){
        this.products.add(product);
    }

}
