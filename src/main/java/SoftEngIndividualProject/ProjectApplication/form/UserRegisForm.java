package SoftEngIndividualProject.ProjectApplication.form;

import com.sun.istack.NotNull;
import lombok.Data;

@Data
public class UserRegisForm {

    private String username;

    private String password;

}
