package SoftEngIndividualProject.ProjectApplication.form;

import lombok.Data;

@Data
public class OrderProductForm {
    private Long cpu_id;
    private Long graphics_card_id;
    private Long motherboard_id;
    private Long ram_id;
    private Long memory_id;
    private Long psu_id;
    private String name;
    private String address;
}
