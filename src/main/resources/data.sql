INSERT INTO public.users(user_id, username, password, authorizations, locked, expired, credentials, enabled) values ('1', 'testCustomer', '$2y$12$hOD6B9bWwu2qtIWqqu1mleagTTs41vWAYSTY1UorXBTU3OlHBrpiG', 'CUSTOMER', true, true, true, true) ON CONFLICT DO NOTHING;
INSERT INTO public.users(user_id, username, password, authorizations, locked, expired, credentials, enabled) values ('2', 'testAdmin', '$2y$12$mCYghRGAAw/M6iwhaWyY7OMKQtBCmaOIYNc1Om.j5KKmbKJBgUHBO', 'ADMIN', true, true, true, true) ON CONFLICT DO NOTHING;

INSERT INTO public.Product(id, product_name, price, information, product_category) values ('1','Intel Core i9-9900K 3.6Ghz Up To 5.0Ghz - Cache 16MB [Box] Socket LGA 1151V2 - Coffeelake Series', '8466000', 'intel core i9', 'processors') ON CONFLICT DO NOTHING;
INSERT INTO public.Product(id, product_name, price, information, product_category) values ('2','Intel Core i9-9900 3.1Ghz Up To 5.0Ghz - Cache 16MB [Box] Socket LGA 1151V2 - Coffeelake Series', '6999000', 'intel core i9', 'processors') ON CONFLICT DO NOTHING;
INSERT INTO public.Product(id, product_name, price, information, product_category) values ('3','Intel Core i7-9700K 3.6Ghz Up To 4.9Ghz - Cache 12MB [Box] Socket LGA 1151V2 - Coffeelake Series', '6287000', 'intel core i7', 'processors') ON CONFLICT DO NOTHING;
INSERT INTO public.Product(id, product_name, price, information, product_category) values ('4','Intel Core i7-9700 3.0Ghz Up To 4.7Ghz - Cache 12MB [Box] Socket LGA 1151V2 - Coffeelake Series', '5544000', 'intel core i7', 'processors') ON CONFLICT DO NOTHING;
INSERT INTO public.Product(id, product_name, price, information, product_category) values ('5','Intel Core i5-9600K 3.7Ghz Up To 4.6Ghz - Cache 9MB [Box] Socket LGA 1151V2- Coffeelake Series', '3490000', 'intel core i7', 'processors') ON CONFLICT DO NOTHING;
INSERT INTO public.Product(id, product_name, price, information, product_category) values ('6','AMD Ryzen 9 3900X 3.8Ghz Up To 4.6Ghz Cache 64MB 105W AM4 [Box] - 12 Core - 100-100000023BOX - With AMD Wraith Stealth Cooler', '7799000', 'AMD Ryzen 9', 'processors') ON CONFLICT DO NOTHING;
INSERT INTO public.Product(id, product_name, price, information, product_category) values ('7','AMD Ryzen 5 3600 3.6Ghz Up To 4.2Ghz Cache 32MB 65W AM4 [Box] - 6 Core - 100-100000031BOX - With AMD Wraith Stealth Cooler', '3079000', 'AMD Ryzen 5', 'processors') ON CONFLICT DO NOTHING;
INSERT INTO public.Product(id, product_name, price, information, product_category) values ('8','AMD Ryzen 5 3500X 3.6Ghz Up To 4.1Ghz Cache 32MB 65W AM4 [Box] - 6 Core - 100-100000158CBX - With AMD Wraith Stealth Cooler', '2268000', 'AMD Ryzen 5', 'processors') ON CONFLICT DO NOTHING;
INSERT INTO public.Product(id, product_name, price, information, product_category) values ('9','AMD Ryzen 5 3500 3.6Ghz Up To 4.1Ghz Cache 16MB 65W AM4 [Box] - 6 Core - 100-100000050BOX - With AMD Wraith Stealth Cooler', '1951000', 'AMD Ryzen 5', 'processors') ON CONFLICT DO NOTHING;
INSERT INTO public.Product(id, product_name, price, information, product_category) values ('10','AMD Ryzen 5 Pinnacle Ridge 2600 3.4Ghz Up To 3.9Ghz Cache 16MB 65W AM4 [Box] - 6 Core - YD2600BBAFBOX With AMD Wraith Stealth Cooler', '1807000', 'AMD Ryzen 5', 'processors') ON CONFLICT DO NOTHING;

INSERT INTO public.Product(id, product_name, price, information, product_category) values ('11','GALAX Geforce RTX 2080 Ti 11GB DDR6 SG Edition - Triple Fan', '17900000', 'RTX 2080Ti', 'graphics_card') ON CONFLICT DO NOTHING;
INSERT INTO public.Product(id, product_name, price, information, product_category) values ('12','GALAX Geforce RTX 2080 SUPER 8GB DDR6 WTF WORK THE FRAMES - 23 RGB Effect - TRIPLE 90mm FAN', '13200000', 'RTX 2080 SUPER', 'graphics_card') ON CONFLICT DO NOTHING;
INSERT INTO public.Product(id, product_name, price, information, product_category) values ('13','GALAX Geforce RTX 2080 SUPER 8GB DDR6 EX (1-Click OC) - RGB Effect - DUAL 100mm FAN', '12200000', 'RTX 2080 SUPER', 'graphics_card') ON CONFLICT DO NOTHING;
INSERT INTO public.Product(id, product_name, price, information, product_category) values ('14','GALAX Geforce RTX 2070 SUPER 8GB DDR6 WTF WORK THE FRAMES - 23 RGB Effect - TRIPLE 90mm FAN', '9500000', 'RTX 2070 SUPER', 'graphics_card') ON CONFLICT DO NOTHING;
INSERT INTO public.Product(id, product_name, price, information, product_category) values ('15','GALAX Geforce RTX 2070 SUPER 8GB DDR6 EX (1-Click OC) - RGB Effect - DUAL 100mm FAN', '8100000', 'RTX 2080Ti', 'graphics_card') ON CONFLICT DO NOTHING;

INSERT INTO public.Product(id, product_name, price, information, product_category) values ('16','GALAX B365M (LGA1151V2, B365, DDR4, USB3.1, SATA3)', '1100000', 'Motherboard', 'motherboards') ON CONFLICT DO NOTHING;
INSERT INTO public.Product(id, product_name, price, information, product_category) values ('17','GALAX H310M (LGA1151V2, H310, DDR4, USB3.1, SATA3)', '790000', 'Motherboard', 'motherboards') ON CONFLICT DO NOTHING;
INSERT INTO public.Product(id, product_name, price, information, product_category) values ('18','GALAX X570M (AM4, AMD Premium X570, DDR4, USB3.1, SATA3) - Promo Limited!!!', '1950000', 'Motherboard', 'motherboards') ON CONFLICT DO NOTHING;
INSERT INTO public.Product(id, product_name, price, information, product_category) values ('19','GALAX B450M (AM4, AMD Promontory B450, DDR4, USB3.1, SATA3)', '990000', 'Motherboard', 'motherboards') ON CONFLICT DO NOTHING;
INSERT INTO public.Product(id, product_name, price, information, product_category) values ('20','ASRock B365 Phantom Gaming 4 (LGA1151V2, B365, DDR4, USB3.1, SATA3)', '1832000', 'Motherboard', 'motherboards') ON CONFLICT DO NOTHING;

INSERT INTO public.Product(id, product_name, price, information, product_category) values ('21','Gskill DDR4 TridentZ Neo RGB PC28800 32GB (2x16GB) Dual Channel F4-3600C16D-32GTZN', '4820000', 'RAM', 'ram') ON CONFLICT DO NOTHING;
INSERT INTO public.Product(id, product_name, price, information, product_category) values ('22','Corsair DDR4 Dominator Platinum RGB PC25600 16GB (2X8GB) - CMT16GX4M2C3200C14', '3955000', 'RAM', 'ram') ON CONFLICT DO NOTHING;
INSERT INTO public.Product(id, product_name, price, information, product_category) values ('23','KLEVV DDR4 CRASS X RGB PC25600 3200MHz 32GB (2X16GB) RGB LED - KD4AGU880-32A160X ( Made In Korea )', '2950000', 'RAM', 'ram') ON CONFLICT DO NOTHING;
INSERT INTO public.Product(id, product_name, price, information, product_category) values ('24','Gskill DDR4 TridentZ Royal PC25600 16GB (2x8GB) Dual Channel F4-3200C16D-16GTRG', '1996000', 'RAM', 'ram') ON CONFLICT DO NOTHING;
INSERT INTO public.Product(id, product_name, price, information, product_category) values ('25','GEIL DDR4 EVO X II RGB LED PC24000 Dual Channel 8GB (2x4GB) 16-18-18-36 GAEXSY48GB3000C16ADC', '980000', 'RAM', 'ram') ON CONFLICT DO NOTHING;

INSERT INTO public.Product(id, product_name, price, information, product_category) values ('26','WDC 500GB SATA3 - Blue', '649000', 'Hard Drive', 'memory') ON CONFLICT DO NOTHING;
INSERT INTO public.Product(id, product_name, price, information, product_category) values ('27','WDC 1TB SATA3 64MB - Blue - WD10EZEX', '679000', 'Hard Drive', 'memory') ON CONFLICT DO NOTHING;
INSERT INTO public.Product(id, product_name, price, information, product_category) values ('28','WDC 2TB SATA3 256MB - Blue - WD20EZAZ', '931000', 'Hard Drive', 'memory') ON CONFLICT DO NOTHING;
INSERT INTO public.Product(id, product_name, price, information, product_category) values ('29','WDC 3TB SATA3 64MB - Red - WD30EFRX (For NAS)', '1370000', 'Hard Drive', 'memory') ON CONFLICT DO NOTHING;
INSERT INTO public.Product(id, product_name, price, information, product_category) values ('30','WDC 4TB SATA3 64MB - Blue - WD40EZRZ', '1633000', 'Hard Drive', 'memory') ON CONFLICT DO NOTHING;

INSERT INTO public.Product(id, product_name, price, information, product_category) values ('31','be quiet! SYSTEM POWER U9 400W - 80+ Bronze Certified - 3 Years Warranty - Number 1 PSU in Germany', '700000', 'Power Supply Unit', 'psu') ON CONFLICT DO NOTHING;
INSERT INTO public.Product(id, product_name, price, information, product_category) values ('32','Seasonic S12III-500 500W - 80+ Bronze Certified - 5 Years Warranty Replacement - Retail Box (NO OEM)', '780000', 'Power Supply Unit', 'psu') ON CONFLICT DO NOTHING;
INSERT INTO public.Product(id, product_name, price, information, product_category) values ('33','Thermaltake Toughpower GF1 Full Modular 650W 80+ Gold (Analog)', '1625000', 'Power Supply Unit', 'psu') ON CONFLICT DO NOTHING;
INSERT INTO public.Product(id, product_name, price, information, product_category) values ('34','Seasonic Focus Gold GX-1000/FX-1000 - 1000W Full Modular - 80+ Gold Certified', '2250000', 'Power Supply Unit', 'psu') ON CONFLICT DO NOTHING;
INSERT INTO public.Product(id, product_name, price, information, product_category) values ('35','Seasonic M12II-520 Evo Edition 520W Full Modular - 80+ Bronze Certified - 5 Years Warranty', '1130000', 'Power Supply Unit', 'psu') ON CONFLICT DO NOTHING;









