>###Faza Aulia Aryoga | 1806173525 - Software Engineering Individual Project

This is the repository for the web application _Compton's Computer_, made for software engineering 2020 individual project  

links:
* [Project Documentation](https://docs.google.com/document/d/1fy6USfM08S-QfvffEA-Cej5Y3RfH3rff2L6URvmARSc/edit?usp=sharing)
* [Daily Work Log](https://docs.google.com/spreadsheets/d/1IVpb5iULpxWq8okNmd873i6zZxazVpc04a3gH-CdXww/edit?usp=sharing)
* [Compton's Computer Website link](https://faza-softeng-indvproject.herokuapp.com)